# Issue basique

<!--
  Nous vous remercions de contribuer à ce projet par la création de cette issue !
  Pour éviter les duplicats, nous vous demandons de cocher les cases dans la liste qui suit.
-->

<!-- Cocher une case doit ressembler à ceci : [x] -->

- [ ] Cette issue est présente dans la dernière version livrée.
- [ ] J'ai cherché dans les [issues](https://gitlab.com/sia-insa-lyon/BdEINSALyon/logos-bde/issues) de ce répertoire et crois que ceci n'est pas un duplicat.

## Contexte 🔦

<!--
  Qu'essayez vous d'accomplir ? Comment le manque de cette fonctionnalité vous a affecté ?
  En donnant du contexte, vous nous aidez à réaliser une solution qui sera la plus utile possible dans le monde réel.
-->

## (Optionnel) Votre environnement 🌎

<!--
  OPTIONNEL - Seulement pour les issues techniques (par exemple : concernant les CI/CD, la documentation, le style/configuration associé au linter ...)
  IMPORTANT - Vous pouvez supprimer cette section et son contenu si vous ne voulez pas la compléter.
  Ajouter autant de détails que vous jugez utile sur l'environnement avec lequel vous avez eu l'issue.
  S'il vous plait, complétez la table qui suit si cette issue relève du domaine technique.
-->
| Env infos        | Version |
| ----------- | ------- |
| Nom du navigateur     |         |
| Sur un smartphone  | :no_entry_sign: or :heavy_check_mark: |

## Actions rapides
<!---Ne touchez pas à cette section sauf si vous savez ce que vous faites. Ces actions rapides sont là pour améliorer le traitement des issues par la suite.--->

/label "Awaiting Feedback"
