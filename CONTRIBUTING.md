## Contributing

Thank you for your interest in contributing to this GitLab project! We welcome
all contributions. By participating in this project, you agree to abide by the
[code of conduct](#code-of-conduct).

## License

By contributing to SIA project, You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to this project.
All Contributions are subject to the following License terms.

[License](./LICENSE)

All Documentation content that resides under the [doc/ directory](/doc) of this
repository is licensed under Creative Commons:
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

_This notice should stay as the first item in the CONTRIBUTING.md file._

## Roles in a SIA Project

A SIA Project should have at least two contributors : a user and a maintainer.

* _User_ : A person that defines the required features, provides feedback on the software, reports usability bugs, basically, someone that actually needs the software, and has good overall knowledge about it.
* _Maintainer_ : A developer (defined below) that is an authority as to what code can be accepted or not into the project. Also, when highly sensitive bugs are discovered, the maintainer accepts to be the first to know and act about it.

Projects with more members may have :

* _Developer_ : A person that is in charge of implementing features and fixing bugs.
* _Reporter_ : A person that is in charge of bringing up issues related to bugs, help testing the software while feature implementations are in progress.

All other people are considered as _Guests_.

## Definition of done

If you contribute to a SIA project please know that changes involve more than just
code. We use the following definition of done.
Your contribution is not *done* until you have made sure it meets all of these
requirements.

1. Clear description explaining the relevancy of the contribution.
1. Working and clean code that is commented where needed. If this is your first contribution, please read the [GitLab JavaScript style guide](https://docs.gitlab.com/ee/development/fe_guide/style/javascript.html).
1. Unit, integration, and system tests that all pass on the CI server.
1. Regressions and bugs are covered with tests that reduce the risk of the issue happening
   again.
1. [Changelog entry added](./CHANGELOG.md), if necessary.
1. Reviewed by relevant (UX/FE/BE/tech writing) reviewers and all concerns are addressed.
1. Confirmed to be working in the Pre-prod stage.
1. Merged by a project maintainer.
1. Added to the release post if relevant.


## I want to contribute!

If you want to contribute to this SIA Project, issues with both the ~"Accepting merge requests" and ~"For Scheduling" labels
are issues that have been triaged as areas where we are looking for contributors to help us out.

See the [contributor issues list](https://gitlab.com/sia-insa-lyon/BdEINSALyon/logos-bde/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Accepting%20merge%20requests&label_name[]=For%20Scheduling).

If you have any questions or need help, contact a member of SIA INSA Lyon.

## Maintainer documentation

Maintainers of this project will try to address issues in a timely manner.
Maintainers, however, cannot guarantee that they would be able to address
all incoming issues as soon as they are raised nor can guarantee to provide an
answer to all raised issues.

If your issue is closed due to inactivity (from either side), please check
whether the issue persists in the latest version. If that is the case, feel free
to reopen the issue or create a new one with a link to the old issue.

### Issue description templates

Issue description template will show this message to
all users that create issues in this repository:

```
When submitting an issue, please check the following:

- [ ] The issue is present in the latest release.
- [ ] I have searched the [issues](https://gitlab.com/sia-insa-lyon/BdEINSALyon/logos-bde/issues) of this repository and believe that this is not a duplicate.

```

English is mandatory only for issues meant for new contributors, French can be used otherwise to simplify the understanding beetween SIA members.

### Issue response template

When the maintainer suspects the reported issue is not related to the problems with this project, following template can be used to respond to the issue reporter:

```
Thanks for reporting this issue. I suspect that the issue you are experiencing is not related to the project or configuration of the project itself.
Since this looks like a problem not related to the project please contact a SIA member for your issue. I will close this issue but if you still think this is a problem with the project please @ mention me with the steps to reproduce the problem and I will reopen the issue.
```

### Closing issues

If an issue has a ~"Awaiting Feedback" label, and the response from the reporter
has not been received for 1 month, we can close the issue using the following
response template:

```
We haven't received an update for more than 14 days so we will assume that the
problem is fixed or is no longer valid. If you still experience the same problem
try upgrading to the latest version. If the issue persists, reopen this issue
with the relevant information.
```

### Reviewing Merge Requests (MR)

Before merging, a MR must be reviewed by a project Maintainer.
If the change is not time-sensitive, then the MR can first be reviewed by any other Dev Team Member then finally reviewed by a Maintainer.

Reviews generally only need the *approval* of one Maintainer to be merged, but
additional Maintainers may be involved with reviewing parts of the change. We will
try to avoid having more than one Maintainer do a full review.

**When the MR affects/answers to an issue that has not been properly defined**

The project users must review the issue formulation, and the maintainer should only review the code itself.

**Other changes**

As long as one of the Maintainers is confident in the change, it does not need to
be reviewed again before merge.

> **Note**: This process is intended to balance code quality with effective use of reviewers' time. Just because a change was approved and merged by one Maintainer, does not guarantee that it won't be reverted later.

## Code of conduct

As contributors and maintainers of this project, we pledge to respect all people
who contribute through reporting issues, posting feature requests, updating
documentation, submitting pull requests or patches, and other activities.

We are committed to making participation in this project a harassment-free
experience for everyone, regardless of level of experience, gender, gender
identity and expression, sexual orientation, disability, personal appearance,
body size, race, ethnicity, age, or religion.

Examples of unacceptable behavior by participants include the use of sexual
language or imagery, derogatory comments or personal attacks, trolling, public
or private harassment, insults, or other unprofessional conduct.

Project maintainers have the right and responsibility to remove, edit, or reject
comments, commits, code, wiki edits, issues, and other contributions that are
not aligned to this Code of Conduct. Contributors involved with the project that do not follow the
Code of Conduct may be removed from the project team.

This code of conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community.

Instances of abusive, harassing, or otherwise unacceptable behavior can be
reported by emailing contact@gitlab.com.

This Code of Conduct is adapted from the [Contributor Covenant][contributor-covenant], version 1.1.0,
available at [http://contributor-covenant.org/version/1/1/0/](http://contributor-covenant.org/version/1/1/0/).

[contributor-covenant]: http://contributor-covenant.org
